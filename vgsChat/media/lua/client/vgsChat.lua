-- require("keyBinding.lua");
-- require MainOptions.lua
local branch;

local keybindInit = function()
    local bind = {};
    bind.value = "vgsChat Menu";
    bind.key = 45;
    for i,v in ipairs(keyBinding) do
        if v.value == "[UI]" then
            table.insert(keyBinding,i+1,bind);
            return;
        end
    end
    table.insert(keyBinding,bind);
end
keybindInit();

-- default table, vgsChatLines.txt overwrites it.
local vgsTable = {
    ["X"] = {
        ["G"] = "I am a potato.",
        ["X"] = {
            ["_category"] = "Very Quick",
            ["H"] = "Help!",
            ["T"] = "Thanks."
        }
    }
};
-- TODO: this should probably use the menu keybind instead of being static
local cancelKey = "ESCAPE";


local function readFile(bReader, current)
    local line = bReader:readLine();
    if line == nil then return; end
    local c = string.sub(line,1,1);
    if c == "+" then
        local o = {};
        o["_category"] = string.sub(line,4);
        local k = string.sub(line,2,2);
        --current[k] = readFile(bReader,o);
        readFile(bReader,o);
        current[k] = o;
        --print("Added Table");
        --table.print(current);
    elseif c == "-" then
        return current;
    else
        current[c] = string.sub(line,3);
        --print("Added String "..tostring(current[c]));
        --table.print(current);
    end
    --return readFile(bReader,current);
    return readFile(bReader,current);
end

local loadLines = function()
    print("---------------------- Start Loading vgsChatLines.txt --------------------------");
    local bReader = getModFileReader("vgsChat","media/vgsChatLines.txt",false);
    --print("bReader = "..tostring(bReader));
    if bReader then
        print("---------------------- Finished Loading vgsChatLines.txt --------------------------");
        print("---------------------- Start Reading vgsChatLines.txt --------------------------");
        local temp = {["X"] = {} };
        readFile(bReader,temp["X"]);
        bReader:close();
        vgsTable = temp;
        print("---------------------- Finished Reading vgsChatLines.txt --------------------------");
    else
        print("---------------------- ERROR Loading vgsChatLines.txt --------------------------");
    end
end

local resetNext = false;
local disabledKeys = {};
-- TODO: might need to use getCore():getKey("keyname") instead of .key
local resetDisabled = function()
    for i=1, #disabledKeys do
        getCore():addKeyBinding(disabledKeys[i].value,disabledKeys[i].key);
    end
    disabledKeys = {};
end

local disableCollisions = function(t)
    if not t then t = branch; end
    for i=1, #MainOptions.keys do
        local option = MainOptions.keys[i];
        if option.key == Keyboard.getKeyIndex(cancelKey) then
--            print("Disable key: "..cancelKey);
            table.insert(disabledKeys,option);
            getCore():addKeyBinding(option.value,255);
        else
            for key,v in pairs(branch) do
                --table.print(MainOptions.keys[i]);
                --print("Key: "..key.." optionkey: "..tostring(option.key));
                if key ~= "_category" and option.key and key == Keyboard.getKeyName(option.key) then
                    -- let's not interfere with movement
                    if option.value == "Forward" or option.value == "Backward"
                        or option.value == "Left" or option.value == "Right" then
                        break;
                    end
--                    print("Disable key: "..key);
                    table.insert(disabledKeys,option);
                    getCore():addKeyBinding(option.value,255);
                    break;
                end
            end
        end
    end
end

local reset = function ()
    branch = nil;
    -- rebind stuff, ugly hack. do it better/faster from memory at least so that it doesn't read a file.
    -- also only want to rebind stuff we unbound
    --MainOptions.loadKeys();
    resetDisabled();
end
local init = function (t)
    branch = t;
    -- unbind stuff, ugly hack. want to make it only unbind collisions.
    --print("keys length: "..#MainOptions.keys);
    disableCollisions(branch);
--[[    for i=1, #MainOptions.keys do
        getCore():addKeyBinding(MainOptions.keys[i].value,255); -- can't use -1, not sure if 0 would match every key event. what to use?
    end
--]]
end

local renderGUI = function ()
    if not branch then return; end
    local textManager = getTextManager();
    local core = getCore();
    -- TODO: if I can detect screen resizes with an event then I should precalculate this there
    --local player = getPlayer();
    -- TODO: clean this up
    local player = getSpecificPlayer(0);
    --local posX = player:getClass():getField("sx"):getInt(player)-IsoCamera:getOffX()-320;
    --local posY = player:getClass():getField("sy"):getInt(player)-IsoCamera:getOffY()-100;
    local posX = isoToScreenX(0, player:getX(), player:getY(), player:getZ())-320;
    local posY = isoToScreenY(0, player:getX(), player:getY(), player:getZ())-100; -- player:getPlayerIndex()

    textManager:DrawString(UIFont.Small, posX + 160, posY, "VGS Chat System", 1, 1, 1, 0.9);
    local i = 1;
    for k,v in pairs(branch) do
        -- TODO: decide on the output format. Also, this might be slow. Maybe precalculate it before render.
        if type(v) == "string" then
            if k ~= "_category" then
                textManager:DrawString(UIFont.Small, posX + 160, posY + i*15, k..": "..v, 1, 1, 1, 0.9);
                i = i + 1;
            end
        else
            textManager:DrawString(UIFont.Small, posX + 160, posY + i*15, "["..k.."]: "..v["_category"], 1, 1, 1, 0.9);
            i = i + 1;
        end
    end
end

-- TODO: reload key still happens?
local function handleKeyInput(keyCode)
    local key = Keyboard.getKeyName(keyCode);
---[[
    if resetNext ~= false then
        getCore():addKeyBinding(resetNext.value,resetNext.key);
        resetNext = false;
    end
--]]
    if MainScreen.instance:getIsVisible() then
        return;
    end
    --print("handleKeyInput "..tostring(key));
    if not branch and keyCode == getCore():getKey("vgsChat Menu") then
        init(vgsTable["X"]);
        return;
    end

    if branch and branch[key] then
        if type(branch[key]) == "string" then
            --print("Output "..branch[key]);
            getPlayer():Say(branch[key]);
--            resetNext = true;
--            branch = nil;
            for i=1, #disabledKeys do
                if disabledKeys[i].key == keyCode then
                    resetNext = disabledKeys[i];
                    break;
                end   
            end
            reset();
            if resetNext ~= false then
                getCore():addKeyBinding(resetNext.value,255);
            end
        else
            resetDisabled();
            branch = branch[key];
            disableCollisions(branch);
            -- update disabled binds?
        end
    elseif key == cancelKey then
        reset();
--        resetNext = true;
--        branch = nil;
--    else if branch then   -- ANY key that's not in the list would reset it with this
--        reset();
        
    end
end

local function addEventHandlers()
    loadLines();
    Events.OnKeyPressed.Add(handleKeyInput);
    Events.OnPostRender.Add(renderGUI);
end

if not isServer() then
    Events.OnGameStart.Add(addEventHandlers)
end