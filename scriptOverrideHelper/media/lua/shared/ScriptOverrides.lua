--[[ THIS LINE IS REQUIRED, DO NOT EDIT IT.
# Copy this file and drop it into your mod's media/lua/shared directory, then edit it as you like.
# If you don't want users to need to install ScriptOverrideHelper then copy ScriptOverrideHelper.lua to media/lua/shared as well.
#   Example comment

#   Example item override, commented
#    item Base.Axe
#    {
#        DisplayCategory = Foobar
#        DoorDamage = 100
#    }
 
# Example recipe override, uncommented
    recipe module.somerecipe
    {
        Foo = Bar        
    }
# THE FOLLOWING LINES ARE REQUIRED, DO NOT EDIT THEM.
--]]