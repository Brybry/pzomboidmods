--[[
    ScriptOverrideHelper.lua by Brybry [briankearney+pzmodding@gmail.com]
    See ScriptOverrides.lua for usage.
--]]
if not ScriptOverrideHelper then
    ScriptOverrideHelper = {};

    ScriptOverrideHelper.isType = function(itemType)
        if not itemType then return false; end

        if itemType == "recipe" then
            return true;
        elseif itemType == "item" then
            return true;
        end
        return false;
    end

    -- could use getScriptManager() instead
    ScriptOverrideHelper.getItem = function(itemType, itemName)
        if not itemType then return nil; end
        
        if itemType == "recipe" then
            return ScriptManager.instance:getRecipe(itemName);
        elseif itemType == "item" then
            return ScriptManager.instance:getItem(itemName);
        end

        return nil;
    end

    ScriptOverrideHelper.doParam = function(itemType, itemName, param)
        local item = ScriptOverrideHelper.getItem(itemType, itemName);
        if not item or not param then
            print("ScriptOverrideHelper WARNING ["..tostring(itemType)..
                "] "..itemName.." => ("..tostring(param)..
                ") failed to DoParam()");
            return;
        end
        item:DoParam(param);
        print("ScriptOverrideHelper Set "..itemType.." "..itemName.." "..param);
    end

    ScriptOverrideHelper.load = function(modId)
        local bReader = getModFileReader(modId, "media/lua/shared/ScriptOverrides.lua", false);

        if not bReader then
            --print("ScriptOverrideHelper WARNING "..tostring(modId).." failed to create reader");
            return;
        end

        local line = bReader:readLine();
        local overrideType = nil;
        local overrideName = nil;
        while line do
            line = string.trim(line);
            
            -- ignore lines that start with comments or brackets and empty lines
            local word = string.sub(line, 1, 1);
            if word ~= "#" and word ~= "{" and word ~= "}" and word ~= "-" and string.len(line) > 0 then
                -- grab the first "word"
                local i = string.find(line, " ");
                if i then
                    word = string.trim(string.sub(line, 1, i));
                end

                if ScriptOverrideHelper.isType(word) then
                    overrideType = word;
                    overrideName = string.trim(string.sub(line,i)); -- rest of line after first "word"
                else -- not a type so it's a parameter
                    if overrideType and overrideName then
                        ScriptOverrideHelper.doParam(overrideType, overrideName, line);
                    end                
                end
            end
            line = bReader:readLine();
        end

    end

    ScriptOverrideHelper.run = function()
        print("The following spam is a normal part of a ScriptOverrideHelper balanced breakfast");
        local list = getModDirectoryTable();
        for i, mod in pairs(list) do
            local modInfo = getModInfo(mod);
            if modInfo and isModActive(modInfo) then
                ScriptOverrideHelper.load(modInfo:getId());
            end
        end
    end

    Events.OnGameBoot.Add(ScriptOverrideHelper.run);
end
