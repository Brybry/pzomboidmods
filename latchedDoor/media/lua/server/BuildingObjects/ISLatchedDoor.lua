--[[
	Mostly copied from Robert Johnson's ISWoodenDoor
	The rest by Brybry (briankearney+pz at gmail)
--]]
-- Could I remove all of this and override the base wooden door since I use moddata so much?
-- this would mean that adding/removing the mod would be less likely to break things.
ISLatchedDoor = ISBuildingObject:derive("ISLatchedDoor");

--************************************************************************--
--** ISLatchedDoor:new
--**
--************************************************************************--
function ISLatchedDoor:create(x, y, z, north, sprite)
	local cell = getWorld():getCell();
	self.sq = cell:getGridSquare(x, y, z);
	local openSprite = self.openSprite;
	if north then
		openSprite = self.openNorthSprite;
	end
	self.javaObject = IsoThumpable.new(cell, self.sq, sprite, openSprite, north, self);
    --[[
    -- find the doorknob that consumeMaterial will remove and use its keyhash
    local knob = getPlayer():getInventory():FindAndReturn("Base.Doornkob");
    if knob ~= nil then
        self.javaObject:getModData()["keyHash"] = knob:getModData()["keyHash"];
    end
    --]]
    buildUtil.setInfo(self.javaObject, self);
	buildUtil.consumeMaterial(self);
	-- set door health
	self.javaObject:setMaxHealth(ISLatchedDoor:getHealth());
	-- the sound that will be played when our door will be broken
	self.javaObject:setBreakSound("breakdoor");
	-- add the item to the ground
	self.sq:AddSpecialObject(self.javaObject);
	buildUtil.addWoodXp();
	self.javaObject:transmitCompleteItemToServer();
end

function ISLatchedDoor:new(sprite, northSprite, openSprite, openNorthSprite)
	local o = {};
	setmetatable(o, self);
	self.__index = self;
	o:init();
	o:setSprite(sprite);
	o:setNorthSprite(northSprite);
	o.openSprite = openSprite;
	o.openNorthSprite = openNorthSprite;
	o.isDoor = true; -- this is probably bad
	o.isLocked = true;
	o.thumpDmg = 5;
	o.name = "Latched Door";

	-- valid types are latch, lock
	o.modData["dtype"] = "latch";
	return o;
end

-- return the health of the new door, + 100 per carpentry lvl
function ISLatchedDoor:getHealth()
	return 250 + buildUtil.getWoodHealth(getPlayer());
end

-- the door can be placed only if it's on a door frame
function ISLatchedDoor:isValid(square, north)
	if not self:haveMaterial(square) then return false end
	if not square:isFreeOrMidair(true, true) then
		return false
	end
	for i = 0, square:getSpecialObjects():size() - 1 do
		local item = square:getSpecialObjects():get(i);
		if instanceof(item, "IsoThumpable") and item:isDoorFrame() and item:getNorth() == north then
			return true;
		end
	end
	for i=0,square:getObjects():size()-1 do
		local o = square:getObjects():get(i)
		if instanceof(o, 'IsoObject') then
			if north and o:getType() == IsoObjectType.doorFrN then
				return true
			end
			if not north and o:getType() == IsoObjectType.doorFrW then
				return true
			end
		end
	end

	return false;
end

-- used to render more tile for example
function ISLatchedDoor:render(x, y, z, square, north)
	ISBuildingObject.render(self, x, y, z, square, north)
end

ISLatchedDoor.onDestroy = function(thump, player)
    print("Destroy test");
    -- check isdoor instead?
    if thump:getName() == "Latched Door" then
        print("Destroy door");
        local modData = thump:getModData();
        if modData["keyHash"] ~= nil then
            print("keyHash == "..tostring(modData["keyHash"]));
            local groundItems = thump:getSquare():getWorldObjects();
            for i = 0, groundItems:size()-1 do
                local item = groundItems:get(i):getItem();
                local iModData = item:getModData();
                if item:getName() == "Doorknob" and iModData["keyHash"] == nil then
                    -- so in theory a player could somehow clone keyhashes right here
                    iModData["keyHash"] = modData["keyHash"];
                    return;
                end
            end

            -- create doorknob if didn't drop, might want to take this out for balance
            local knob = InventoryItemFactory.CreateItem("Base.Doorknob");
            knob:getModData()["keyHash"] = modData["keyHash"];
            thump:getSquare():AddWorldInventoryItem(knob,0.0,0.0,0.0);
            --thump:getSquare():AddWorldInventoryItem("Base.Doorknob",0.0,0.0,0.0);
        end
    end
end

-- sadly more client authoritative, doesn't fire on server
if not isServer() then
    Events.OnDestroyIsoThumpable.Add(ISLatchedDoor.onDestroy);
end

ISLatchedDoor.onLatch = function(worldobjects, door)
--print("onLatch() fired!");
	if luautils.walkAdjWindowOrDoor(door:getSquare(), door) then
--print("onLatch() setLocked!");
        -- TODO: make this a timed action
		door:setIsLocked(true);
		--door:syncIsoObject(false,0,nil);
		local data = {};
		data.x = door:getSquare():getX();
		data.y = door:getSquare():getY();
		data.z = door:getSquare():getZ();
		data.i = door:getSquare():getObjects():indexOf(door);
		sendClientCommand("latchedDoor", "lock", data);
	end
end

ISLatchedDoor.onUnlock = function(worldobjects, door)
--print("onUnlock() fired!");
    if luautils.walkAdjWindowOrDoor(door:getSquare(), door) then
        -- TODO: make this a timed action
        --print("onUnlock() setLocked!");
        door:setIsLocked(false);
        --door:syncIsoObject(false,0,nil);
        local data = {};
        data.x = door:getSquare():getX();
        data.y = door:getSquare():getY();
        data.z = door:getSquare():getZ();
        data.i = door:getSquare():getObjects():indexOf(door);
        sendClientCommand("latchedDoor", "unlock", data);
    end
end

ISLatchedDoor.changeLockMenu = function(subMenu, player, door)
    -- I wonder if I could cache this and then use a hook in inventorytransferaction to invalidate the cache
    local doorknobs = getSpecificPlayer(player):getInventory():FindAll("Doorknob");
    for i = 0, doorknobs:size()-1 do
        local knob = doorknobs:get(i);
        local keyHash = knob:getModData()["keyHash"];
        if keyHash ~= nil then
            subMenu:addOption("Doorknob"..tostring(keyHash), worldobjects, ISLatchedDoor.onChangeLock, player, door, knob);
        end
    end
end

ISLatchedDoor.onChangeLock = function(worldobjects, player, door, knob)
print("onChangeLock() fired!");
	if luautils.walkAdjWindowOrDoor(door:getSquare(), door) then
        -- TODO: make this a timed action
        local modData = door:getModData();
		if modData["dtype"] == "latch" then
            --local testHash = getSpecificPlayer(player):getInventory():FindAndReturn("Doorknob"):getModData()["keyHash"];
            -- when this is a timed action make sure to double check that the player still has the doorknob!
            -- I think there is a find(object)
            local knobHash = knob:getModData()["keyHash"];
            print("Debug testhash"..tostring(knobHash));
			modData["dtype"] = "lock";
            modData["keyHash"] = knobHash;
            -- should I use a ISInventoryTransferAction for the removal?
            -- DoRemoveItem() instead?
            getSpecificPlayer(player):getInventory():Remove(knob);
            --modData["knob"] = knob; -- only adds on local client
		else
			modData["dtype"] = "latch";
            --if modData["knob"] == nil then
                print("knob nil");
                -- client authoritative again
                local knob = InventoryItemFactory.CreateItem("Base.Doorknob");
                knob:getModData()["keyHash"] = modData["keyHash"];
                getSpecificPlayer(player):getInventory():AddItem(knob);
            --else
                --getSpecificPlayer(player):getInventory():AddItem(modData["knob"]);
            --end
            modData["keyHash"] = nil;
            -- gi
		end

		door:setModData(modData); -- not needed?
		--door:transmitModData(); -- pretty sure this doesn't work from client
		local data = {};
		data.x = door:getSquare():getX();
		data.y = door:getSquare():getY();
		data.z = door:getSquare():getZ();
		data.i = door:getSquare():getObjects():indexOf(door);
		data.modData = modData;
		sendClientCommand("latchedDoor", "modData", data);
	end
end

ISLatchedDoor.canUnlockDoor = function(player, keyHash)
    if keyHash == nil then
        return true;
    end
    -- maybe look at FindAndReturnStack
    -- FindAll("Key")
    local keys = getSpecificPlayer(player):getInventory():FindAll("latchedDoor.Key");
--    if keys:isEmpty() then
--        return false;
--    end
    for i = 0, keys:size()-1 do
        local key = keys:get(i):getModData()["keyHash"];
        if key ~= nil and key == keyHash then
            return true;
        end
    end
    return false;
end

ISLatchedDoor.keyPressed = false;
ISLatchedDoor.doKludgeSupport = function(key)
	if key == 18 then
		ISLatchedDoor.keyPressed = true;
	end
end
ISLatchedDoor.doKludgeMouseSupport = function()
	ISLatchedDoor.keyPressed = true;
end

--ISLatchedDoor.i = 0;
-- Goes through every nearby open and locked door and sets it to unlocked because locked open doors are silly
ISLatchedDoor.doKludge = function()
--	ISLatchedDoor.i = ISLatchedDoor.i + 1;
--	print("-------------doKludge--------------"..tostring(ISLatchedDoor.i));
	if ISLatchedDoor.keyPressed ~= true then
--		print("No key -- return");
		return;
	end
	ISLatchedDoor.keyPressed = false;
	local player = getPlayer();
	local checkForDoorsAndWindows = player:getCurrentSquare();
	if (checkForDoorsAndWindows ~= nil) then
		local d = checkForDoorsAndWindows:getDoorOrWindow(false);
		if (d ~= nil and d:getName() == "Latched Door") and d:IsOpen() and d:isLocked() then
--			print("unlocked me. [aka W]");
			d:setIsLocked(false);
--			door:syncIsoObject(false,1,nil);
		end
		local d = checkForDoorsAndWindows:getDoorOrWindow(true);
		if (d ~= nil and d:getName() == "Latched Door") and d:IsOpen() and d:isLocked() then
--			print("unlocked me2. [aka N]");
			d:setIsLocked(false);
--			door:syncIsoObject(false,1,nil);
		end
	end
	checkForDoorsAndWindows = player:getCell():getGridSquare(player:getX(),player:getY()+1.0,player:getZ());
	if (checkForDoorsAndWindows ~= nil) then
		local d = checkForDoorsAndWindows:getDoorOrWindow(true);
		if (d ~= nil and d:getName() == "Latched Door") and d:IsOpen() and d:isLocked() then
--			print("unlocked north [aka S].");
			d:setIsLocked(false);
--			door:syncIsoObject(false,1,nil);
		end
	end
	checkForDoorsAndWindows = player:getCell():getGridSquare(player:getX()+1.0,player:getY(),player:getZ());
	if (checkForDoorsAndWindows ~= nil) then
		local d = checkForDoorsAndWindows:getDoorOrWindow(false);
		if (d ~= nil and d:getName() == "Latched Door") and d:IsOpen() and d:isLocked() then
--			print("unlocked east2 aka E.");
			d:setIsLocked(false);
--			door:syncIsoObject(false,1,nil);
		end
	end
end

ISLatchedDoor.findDoorknobInventory = function()
    local doorknob = nil;
    return doorknob;
end

ISLatchedDoor.canChangeLock = function(player, door)
    return true;
end

-- might want to put this elsewhere? not sure. don't really need to create this function for every door
ISLatchedDoor.doLatchedMenu = function(player, context, worldobjects, test)
print("doLatchedMenu");
    local lDoor = nil;
	local square = nil;
	for _,v in ipairs(worldobjects) do
		square = v:getSquare();
  		if instanceof(v, "IsoDoor") or (instanceof(v, "IsoThumpable") and v:isDoor()) then
			lDoor = v;
            break;
		end
	end
--	print("Player in room? " .. (getSpecificPlayer(player):getCurrentSquare():getRoom() ~= nil and "true" or "false"));
	if lDoor == nil or square == nil then
		return true
	end
	print("Door locked? " .. tostring(lDoor:isLocked()));
	print("Door open? "..tostring(lDoor:IsOpen()));
	print("Door modData = "..table.tostring(lDoor:getModData()));-- object getTable setTable thumpable setModData
--	print("Door name: "..tostring(lDoor:getName()));

	if lDoor:getName() == "Latched Door" then
        local dtype = lDoor:getModData()["dtype"];
        if getSpecificPlayer(player):getCurrentSquare():getRoom() ~= nil then
            -- TODO: maybe move this to build menu/ISBuildMenu_LD.lua
            -- TODO: maybe make this so you can do this from either side but only if the door is open?
            -- TODO: balance/carpentry req/recipe
            -- TODO: maybe set door to unlocked when changing type
            -- TODO: removing keylock should give back the doorknob!
            --[[
                if can change lock (has doorknob + hammer in inventory)
                if type == latch
                - remove latch
                - add key lock >
                    - subcontext menu with list of doorknobs
                    - see ISBuildMenu.canBuild(0,0,0,1,0,0,doorsOption, player);
                    - in ISBuildMenu_LD.lua!
                if type == key lock
                    - remove lock
                - in future maybe remove type and just have a latched variable and then keyhash variable
                - that way I can have the same door be both latching and locked
             ]]
            if  dtype == "latch" then
                --context:addOption("Add key lock", worldobjects, ISLatchedDoor.onChangeLock, player, lDoor);
                local changeOption = context:addOption("Add key lock", worldobjects, nil);
                local subMenu = ISContextMenu:getNew(context);
                context:addSubMenu(changeOption, subMenu);
                ISLatchedDoor.changeLockMenu(subMenu,player,lDoor)
            else
                context:addOption("Remove key lock", worldobjects, ISLatchedDoor.onChangeLock, player, lDoor);
            end

            if not lDoor:IsOpen() then
                -- fixes can't open door from gui when inside+locked bug in base game
                if lDoor:isLocked() then -- closed, locked, in room
                    local f = false;
                    local openText = getText("ContextMenu_Open_door");
                    for _,option in pairs(context.options) do
                        if option.name == openText then f = true; break; end
                    end
                    if not f then
                        context:addOption(openText, worldobjects, ISWorldObjectContextMenu.onOpenCloseDoor, door);
                    end
--[[
                    if dtype == "lock" then
                        context:addOption("Unlock door", worldobjects, ISLatchedDoor.onUnlock, lDoor);
                    end
--]]
                else -- closed, unlocked, in room
                    if dtype == "latch" then
                        context:addOption("Latch door", worldobjects, ISLatchedDoor.onLatch, lDoor);
                    else
                        context:addOption("Lock door", worldobjects, ISLatchedDoor.onLatch, lDoor);
                    end
                end
            end
        -- not in room, closed
        elseif dtype == "lock" and not lDoor:IsOpen() then
            -- TODO: maybe allow player to straight up open locked doors ('E' hooking performance issues? but click easy)
            if ISLatchedDoor.canUnlockDoor(player, lDoor:getModData()["keyHash"]) then
                if lDoor:isLocked() then
                    context:addOption("Unlock door", worldobjects, ISLatchedDoor.onUnlock, lDoor);
                else
                    context:addOption("Lock door", worldobjects, ISLatchedDoor.onLatch, lDoor);
                end
            end
        end

	end
end

if not isServer() then
-- OnPreFill is another option
Events.OnFillWorldObjectContextMenu.Add(ISLatchedDoor.doLatchedMenu);
-- after every container update if player did mouse or e action then check for locked open doors. dumb.
Events.OnContainerUpdate.Add(ISLatchedDoor.doKludge); -- because still locked after opening. magical.
Events.OnKeyPressed.Add(ISLatchedDoor.doKludgeSupport);
Events.OnMouseDown.Add(ISLatchedDoor.doKludgeMouseSupport);
-- TODO: catch mouse event and unlock doors if proper to do so
end


ISLatchedDoor.serverGetDoor = function(x,y,z,i)
	local square = getCell():getGridSquare(x,y,z);
	local door = nil;
	if square ~= nil and i >= 0 and i < square:getObjects():size() then
		door = square:getObjects():get(i);
    end

	return door,square;
end
ISLatchedDoor.syncObject = function(mName,command,player,args)
	if mName ~= "latchedDoor" then
		return true;
    end
--	print("latchedDoor syncObject");
	if isServer() then
		if command == "lock" then
			print("latchedDoor lock args = "..table.tostring(args));
            local door, square = ISLatchedDoor.serverGetDoor(args.x,args.y,args.z,args.i);
			if door ~= nil and square ~= nil then
			door:setIsLocked(true);
			-- makes a duplicate on nearby clients
			door:transmitCompleteItemToClients(); -- would syncing myself be faster?
			-- removes old from nearby clients
			square:transmitRemoveItemFromSquare(door); -- removes from server and nearby clients
			-- adds back to server
			square:getObjects():add(door);
			square:getSpecialObjects():add(door);
            end
        elseif command == "unlock" then
            print("latchedDoor unlock args = "..table.tostring(args));
            local door, square = ISLatchedDoor.serverGetDoor(args.x,args.y,args.z,args.i);
            if door ~= nil and square ~= nil then
                door:setIsLocked(false);
                -- makes a duplicate on nearby clients
                door:transmitCompleteItemToClients(); -- would syncing myself be faster?
                -- removes old from nearby clients
                square:transmitRemoveItemFromSquare(door); -- removes from server and nearby clients
                -- adds back to server
                square:getObjects():add(door);
                square:getSpecialObjects():add(door);
           end
        elseif command == "modData" then
			print("latchedDoor modData args = "..table.tostring(args));
			local door, square = ISLatchedDoor.serverGetDoor(args.x,args.y,args.z,args.i);
			if door ~= nil and square ~= nil then
			-- should I trust the client? it could be setting dtype to a bad value!
--[[			if args.modData == nil or type(args.modData) ~= "table" then
				return true;
			end
--]]			door:setModData(args.modData);
			door:transmitModData();
			end
		end
	end
end
--[[
-- for the future if I have to do syncing myself instead of the kludge remove/add method
if isClient() then 
Events.OnServerCommand.Add(ISLatchedDoor.syncObject);
end
--]]
if isServer() then
Events.OnClientCommand.Add(ISLatchedDoor.syncObject);
end

--[[
moddata table per door to store 'key' and door level
unlock from outside via context menu (not in room!)
lock from outside via context menu (not in room!)
change 'latch door' to 'lock door' when door upgraded (or maybe only if you have a key?)
with proper tools maybe able to right click to upgrade it, sort of like a recipe?
upgraded door will have setting in moddata saying that it's turnkey door
must find keys in houses? how to make realistic. can I make the doorknobs that drop sometimes 'special' doorknobs?
then those special doorknobs are used to upgrade the latched door?

carpentry max then mouseover door shows a tooltip with door health and lets you repair it?
catch right mouse click to destroy the tooltip so context menus aren't obscured?

lockable/unlockable containers: two differenty type of containers. one is a dummy. then swap them maybe?
that's for like crates...
might be easier with bags. might be able to just override context menus to do what I want.

some sort of weird visibility bug, spawn into a room with a closed latcheddooor

key graphic public domain from http://opengameart.org/content/modern-key-icons

padlock? http://www.iconarchive.com/show/ravenna-3d-icons-by-double-j-design/Lock-icon.html
combo lock? http://www.iconarchive.com/show/mac-icons-by-artua/Lock-icon.html
other combo lock? http://www.iconarchive.com/show/soda-red-icons-by-trysoda/Lock-icon.html
padlock? http://commons.wikimedia.org/wiki/File:Farm-Fresh_lock.png
freeware padlock? http://www.iconarchive.com/show/my-seven-icons-by-itzikgur/Keys-icon.html
gaudy padlock http://www.iconarchive.com/show/e-commerce-icons-by-ebiene/padlock-lock-icon.html
alternate key http://www.iconarchive.com/show/stainless-icons-by-iconleak/key-icon.html
non-commercial key http://www.iconarchive.com/show/refresh-cl-icons-by-tpdkdesign.net/System-Key-icon.html
non-commercial key http://www.iconarchive.com/show/software-icons-by-aha-soft/key-icon.html

maybe split the key/doornknob code out if possible into a separate mod
that way other people can make mods that use keys/doornkobs but not need the door stuff

can I extend the mod loader to make a framework for uninstalling mods?
that way mods could be tested and not have to worry about trashing player/map saves through items that no longer exist?

srever verified persistent skills mod
server side character saves mod
--]]