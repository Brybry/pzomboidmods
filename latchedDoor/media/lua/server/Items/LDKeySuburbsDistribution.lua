--[[require "Items/SuburbsDistributions"

if SuburbsDistributions["bedroom"] == nil then
    SuburbsDistributions["bedroom"] = {};
end
if SuburbsDistributions["bedroom"]["all"] == nil then
    SuburbsDistributions["bedroom"]["all"] = {};
end
if SuburbsDistributions["bedroom"]["all"].rolls == nil then
    SuburbsDistributions["bedroom"]["all"].rolls = 1;
end
if SuburbsDistributions["bedroom"]["all"].items == nil then
    SuburbsDistributions["bedroom"]["all"].items = {};
end
table.insert(SuburbsDistributions["bedroom"]["all"].items,"latchedDoor.Key");
table.insert(SuburbsDistributions["bedroom"]["all"].items, 90);


if SuburbsDistributions["livingroom"] == nil then
    SuburbsDistributions["livingroom"] = {};
end
if SuburbsDistributions["livingroom"]["all"] == nil then
    SuburbsDistributions["livingroom"]["all"] = {};
end
if SuburbsDistributions["livingroom"]["all"].rolls == nil then
    SuburbsDistributions["livingroom"]["all"].rolls = 1;
end
if SuburbsDistributions["livingroom"]["all"].items == nil then
    SuburbsDistributions["livingroom"]["all"].items = {};
end
table.insert(SuburbsDistributions["livingroom"]["all"].items,"latchedDoor.Key");
table.insert(SuburbsDistributions["livingroom"]["all"].items, 90);

if SuburbsDistributions["diningroom"] == nil then
    SuburbsDistributions["diningroom"] = {};
end
if SuburbsDistributions["diningroom"]["all"] == nil then
    SuburbsDistributions["diningroom"]["all"] = {};
end
if SuburbsDistributions["diningroom"]["all"].rolls == nil then
    SuburbsDistributions["diningroom"]["all"].rolls = 1;
end
if SuburbsDistributions["diningroom"]["all"].items == nil then
    SuburbsDistributions["diningroom"]["all"].items = {};
end
table.insert(SuburbsDistributions["diningroom"]["all"].items,"latchedDoor.Key");
table.insert(SuburbsDistributions["diningroom"]["all"].items, 90);

if SuburbsDistributions["kitchen"] == nil then
    SuburbsDistributions["kitchen"] = {};
end
if SuburbsDistributions["kitchen"]["all"] == nil then
    SuburbsDistributions["kitchen"]["all"] = {};
end
if SuburbsDistributions["kitchen"]["all"].rolls == nil then
    SuburbsDistributions["kitchen"]["all"].rolls = 1;
end
if SuburbsDistributions["kitchen"]["all"].items == nil then
    SuburbsDistributions["kitchen"]["all"].items = {};
end
table.insert(SuburbsDistributions["kitchen"]["all"].items,"latchedDoor.Key");
table.insert(SuburbsDistributions["kitchen"]["all"].items, 90);

if SuburbsDistributions["bathroom"] == nil then
    SuburbsDistributions["bathroom"] = {};
end
if SuburbsDistributions["bathroom"]["all"] == nil then
    SuburbsDistributions["bathroom"]["all"] = {};
end
if SuburbsDistributions["bathroom"]["all"].rolls == nil then
    SuburbsDistributions["bathroom"]["all"].rolls = 1;
end
if SuburbsDistributions["bathroom"]["all"].items == nil then
    SuburbsDistributions["bathroom"]["all"].items = {};
end
table.insert(SuburbsDistributions["bathroom"]["all"].items,"latchedDoor.Key");
table.insert(SuburbsDistributions["bathroom"]["all"].items, 90);

if SuburbsDistributions["hall"] == nil then
    SuburbsDistributions["hall"] = {};
end
if SuburbsDistributions["hall"]["all"] == nil then
    SuburbsDistributions["hall"]["all"] = {};
end
if SuburbsDistributions["hall"]["all"].rolls == nil then
    SuburbsDistributions["hall"]["all"].rolls = 1;
end
if SuburbsDistributions["hall"]["all"].items == nil then
    SuburbsDistributions["hall"]["all"].items = {};
end
table.insert(SuburbsDistributions["hall"]["all"].items,"latchedDoor.Key");
table.insert(SuburbsDistributions["hall"]["all"].items, 90);
--]]

-- I think this works like a closure but I'm not sure it matters
-- testing seemed to show that it's already inside a closure
do
local roomChance = {
    ["bedroom"] = 10,
    ["livingroom"] = 10,
    ["diningroom"] = 5,
    ["kitchen"] = 5,
    ["bathroom"] = 5,
    ["hall"] = 5,
    ["all"] = 1
};

local validContainers = {
    "counter",
    "sidetable",
    "wardrobe",
    "bedroomdrawers",
    "inventoryfemale",
    "inventorymale"
};

local isValidContainer = function(containerType)
    for i = 1, #validContainers do
        if validContainers[i] == containerType then
            return true;
        end
    end
    return false;
end

local buildingDefHasRoom = function (roomName)
    return false;
end
local getClosestResidenceZone = function(zone, x, y)
    local buildings = cell:getClass():getField("buildings"):get(cell); -- actually a zone
    if not buildings or buildings:size() == 0 then
        print("zone has no buildings");
        local adjacent = cell:getClass():getField("adjacent"):get(cell);
        for i = 0, adjacent:size()-1 do
            buildings = cell:getClass():getField("buildings"):get(cell); -- actually a zone
            if buildings and buildings:size() > 0 then
                print("found adjacent zone with buildings");
                break;
            end
        end
    end
    if (not buildings) then
        print("no buildings");
        return nil;
    end
    local closest;
    local distance = 99999;
    print("buildings: "..tostring(buildings:size()));

    -- 0:0 cell if server, bleh
    --print("cell "..tostring(cell:getWorldX())..":"..tostring(cell:getWorldY()));
    for i = 0, buildings:size()-1 do
        local building = buildings:get(i);

        print("building debug "..tostring(building));
        if buildingDefHasRoom(building:isResidential()) then
            local bdef = building;
            local tDist = IsoUtils:DistanceTo(x,y,bdef:getX2() - (bdef:getW()/2),bdef:getY2()- (bdef:getH()/2));
            print("residential building["..tostring(i).."] dist "..tostring(tDist));
            if tDist < distance then
                closest = building;
                distance = tDist;
            end

        end
    end
    return closest;
end

local getClosestResidence = function(cell, x, y)
    local buildings;
    if isServer() then
        buildings = cell:getClass():getField("buildings"):get(cell); -- actually a zone
        if not buildings or buildings:size() == 0 then
            print("zone has no buildings");
            local adjacent = cell:getClass():getField("adjacent"):get(cell);
            for i = 0, adjacent:size()-1 do
                buildings = cell:getClass():getField("buildings"):get(cell); -- actually a zone
                if buildings and buildings:size() > 0 then
                    print("found adjacent zone with buildings");
                    break;
                end
            end
        end
    else
        buildings = cell:getBuildingList();
    end
    if (not buildings) then
        print("no buildings");
        return nil;
    end
    local closest;
    local distance = 99999;
    print("buildings: "..tostring(buildings:size()));

    -- 0:0 cell if server, bleh
    --print("cell "..tostring(cell:getWorldX())..":"..tostring(cell:getWorldY()));
    for i = 0, buildings:size()-1 do
        local building = buildings:get(i);

        print("building debug "..tostring(building));
        if building:isResidential() then
            local bdef = building:getClass():getField("def"):get(building);
            local tDist = IsoUtils:DistanceTo(x,y,bdef:getX2() - (bdef:getW()/2),bdef:getY2()- (bdef:getH()/2));
            print("residential building["..tostring(i).."] dist "..tostring(tDist));
            if tDist < distance then
                closest = building;
                distance = tDist;
            end

        end
    end
    return closest;
end

-- TODO: I could add the key hash here instead
local spawnItems = function(roomName, containerType, container)
    ---[[
    if not roomName or type(roomName) ~= "string" then
        print("no roomname!");
        return;
    end--]]

    local chance = roomChance[roomName];
    if chance and ZombRand(100) < chance and isValidContainer(containerType) then
        print("Add key: "..tostring(container:getSourceGrid():getX())..":"..tostring(container:getSourceGrid():getY())
            ..":"..tostring(container:getSourceGrid():getZ())
        );
--[[        if (roomName == "all") then -- zombie
            print("make zombie key if possible");
            local zone = container:getSourceGrid():getZone();
            if zone then
                print("zone! b "..tostring(zone:getClass():getField("buildings"):get(zone):size()).." a "..
                    tostring(zone:getClass():getField("adjacent"):get(zone):size())
                )
            end
            -- TODO: test that this actually works in singleplayer
            -- TODO: the problem here is that getCell() returns 0,0 on a server so have to use zones w/ bdefs instead?
            local cellOrZone = container:getSourceGrid():getCell();
            if isServer() then
                cellOrZone = container:getSourceGrid():getZone();
            end
            local building = getClosestResidence(cellOrZone,
                container:getSourceGrid():getX(),container:getSourceGrid():getY());
            if not building then print("no building found") return; end
            local room = building:getRandomRoom("kitchen");
            if not room then print("no kitchen found") return; end
            local square = room:getSquares():get(0);
            if not square then print("kitchen has no tiles") return; end
            local key = instanceItem("latchedDoor.Key");
            local modData = key:getModData();
            modData.keyHash = "x"..tostring(square:getX()).."y"..tostring(square:getY()).."z"..tostring(square:getZ());
            print("new keyHash "..modData.keyHash);
            container:AddItem(key);
        else--]]
            container:AddItem("latchedDoor.Key");
--        end

        --print("Spawned key.")
    end
end

-- only fill on server and single player
if not isClient() then
    Events.OnFillContainer.Add(spawnItems)
end
end

--[[
-- don't spawn if square:getZone():haveCons() ?
-- could use container:getSourceGrid():getRoom():getContainer():get(0); to limit to one container per room
--livingroom
--diningroom
--kitchen
--bathroom
--hall
--counter/sidetable/wardrobe/bedroomdrawers
-- zombies are 'all' and 'inventorymale' and 'inventoryfemale' container type
--]]