if table.pack == nil then
    table.pack = function(...)
        return { n = select("#", ...), ... }
    end
end

local ISInventoryTransferAction_new_old = ISInventoryTransferAction.new;
--function ISInventoryTransferAction:new(character, item, srcContainer, destContainer, time)
function ISInventoryTransferAction:new(...)
    local args = table.pack(...); -- this might be too slow to use this way
    if args[1] ~= nil and args[2] ~= nil then
        local char = args[1];
        local item = args[2];
        local itemName = item:getScriptItem():getFullName();
        print("item data "..itemName.." "..table.tostring(item));
        if itemName == "Base.Doorknob" or itemName == "latchedDoor.Key" then
            ISInventoryTransferAction.updateKeyOrDoorknobHash(item, char);
        end
    end
    return ISInventoryTransferAction_new_old(self, ...);
end

-- TODO: This is client authoritative but I do what I must
-- TODO: potential bug where the roomdef changes and so the hash won't be the top left corner of the room
-- or it might be that the cell isn't loaded yet so it might not be in the list of room cells? can I use building bounds?
ISInventoryTransferAction.updateKeyOrDoorknobHash = function(item, char)
    local modData = item:getModData();
    print("modData "..table.tostring(modData));
    -- already set hash, return.
    if modData.keyHash ~= nil then return; end
    local square = char:getCurrentSquare();
    print("current location x"..square:getX().."y"..square:getY());
--[[
Cell()
getRoomList
getBuildingList
getClosestBuildingExcept(isochar, room)

getBestBuildings
getBuildingList

-- also can pull from servermap
getCell():getGridSquare
getWorld():getCell():getGridSquare

    if not a room do something like: (kudos blindcoder)
    (x, y) = (GetRandom(map.maxX), GetRandom(map.maxY))
    if (isHouseAt(x, y)){ ID = createIDForHouseAt(x, y) } else { ID = -1 }

-- zombie key should be nearest valid building as then it's gameable!

--]]
    if square:getRoom() == nil then return; end
    local building = square:getRoom():getBuilding();
    local idRoom = building:getRandomRoom("kitchen"); -- pray it only has one kitchen I guess
    if idRoom == nil then return; end
    local idRoomSq = idRoom:getSquares():get(0);
    if idRoomSq == nil then return; end
    modData.keyHash = "x"..tostring(idRoomSq:getX()).."y"..tostring(idRoomSq:getY()).."z"..tostring(idRoomSq:getZ());
    print("new keyHash "..modData.keyHash);
end

function ISInventoryPaneContextMenu:onRenameLDClick(button)
    if button.internal == "OK" then
        if button.parent.entry:getText() and button.parent.entry:getText() ~= "" then
            button.parent.item:setName(button.parent.entry:getText());
            button.parent.item:setCustomName(true);
            local pdata = getPlayerData(button.parent.character:getPlayerNum());
            pdata.playerInventory:refreshBackpacks();
            pdata.lootInventory:refreshBackpacks();
        end
    end
end
-- context:addOption(getText("ContextMenu_RenameBag"), canBeRenamed, ISInventoryPaneContextMenu.onRenameBag);
ISInventoryPaneContextMenu.onRenameLD = function(item)
    -- getText("ContextMenu_NameThisBag")
    -- item:getScriptItem():getName()
    local modalStr = "Name this " .. item:getScriptItem():getDisplayName() .. " : " .. item:getName();
    local modal = ISTextBox:new(0, 0, 280, 180,  modalStr, nil, ISInventoryPaneContextMenu.onRenameLDClick,
        getSpecificPlayer(ISContextMenu.globalPlayerContext), item, item:getName());
    modal:initialise();
    modal:addToUIManager();
end

ISInventoryPaneContextMenu.LDInventoryKeyHook = function(player, context, items)
    print("LDInventoryKeyHook fired");
    local square = getPlayer():getCurrentSquare();
    print("current location x"..square:getX().."y"..square:getY());
    print("cell "..tostring(getCell():getWorldX())..":"..tostring(getCell():getWorldY()));
    if items == nil or #items > 1 or not items[1] then
        return;
    end
    local item;
    local name;

    if items[1].count ~= nil then
        name = items[1].items[1]:getScriptItem():getFullName();
        item = items[1].items[1];
    else
        name = items[1]:getScriptItem():getFullName();
        item = items[1];
    end

--[[
    -- this is weird and I need an adult. doesn't really get what the player clicks
    -- maybe there's a faster way to do this, like hook onclick to grab the specific slot
    local found = false;
    for k,i in pairs(items) do
        if i.count ~= nil then
            print("#"..tostring(k).." ["..i.items[1]:getScriptItem():getFullName().."] \""..i.items[1]:getName().."\" C: "..
                tostring(i.count)); if not found then
            name = i.items[1]:getScriptItem():getFullName();
            if name == "Base.Doorknob" or name == "latchedDoor.Key" then
                item = i.items[1];
                found = true;
                --break;
            end end
        else
            print("#"..tostring(k).." ["..i:getScriptItem():getFullName().."] \""..i:getName().."\"");
            if not found then
            name = i:getScriptItem():getFullName();
            if name == "Base.Doorknob" or name == "latchedDoor.Key" then
                item = i;
                found = true;
                --break;
            end end
        end
    end

    if item == nil then
        return;
    end
--]]
    print("Grr.");
    --table.print_r(items);
    --[[ local item = items[1];
     if items[1].count ~= nil then
         item = items[1]
     end
     --]]
    --[[
        items = { 1 -> java.ComboItem }
        items = { 1 -> {cat->"Item",count->"2",items {1,2} ,name->"Latched Door",invPanel {}}


    --]]
    --local name = item:getScriptItem():getFullName();
    print("Item! ["..name.."] "..item:getName());
    if name == "Base.Doorknob" or name == "latchedDoor.Key" then
        context:addOption("Rename", item, ISInventoryPaneContextMenu.onRenameLD);
    end
   --[[ print("items");
    table.print_r(items);
    print("mod data");
    print(items[1]);
    print("Test 1"..items[1]:getScriptItem():getDisplayName());
    print("Test 2"..items[1]:getScriptItem():getName());
    local foobar = items[1]:getModData();
    if not foobar.foo or type(foobar.foo) ~= "number" then
        foobar["foo"] = 1;
    else
        foobar.foo = foobar.foo + 1;
    end--]]
    --items[1]:setName(tostring(foobar["foo"]));
    --items[1]:setCustomName(true);
    --local pdata = getPlayerData(button.parent.character:getPlayerNum());
    --pdata.playerInventory:refreshBackpacks();
    --pdata.lootInventory:refreshBackpacks();
    --table.print_r(foobar);
    --table.print_r(items[1]:getModData());
end
Events.OnFillInventoryObjectContextMenu.Add(ISInventoryPaneContextMenu.LDInventoryKeyHook);
--[[

item
setDisplayName()
getDisplayName()
getName()
setName()
getModuleName()
getFullName()
getSpriteName()
setSpriteName()

inventoryitem
setCustomName(Bool)
setName()
isCustomName()
getDisplayName()
getScriptItem()?

--]]