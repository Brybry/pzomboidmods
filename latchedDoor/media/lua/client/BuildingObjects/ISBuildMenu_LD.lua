--[[
	By Brybry (briankearney+pz at gmail)
--]]
-- ISBuildMenu.cheat = true; -- debug
-- --if you ever need an item Events.OnCreatePlayer.Add(function(id) getSpecificPlayer(id):getInventory():AddItem("Base.Battery"); end)
-- TODO: should use a different object in case other mods hook and accidentally use the same function
ISBuildMenu.buildDoorMenuOld = ISBuildMenu.buildDoorMenu;
ISBuildMenu.buildDoorMenu = function(subMenu, player)
	print("Build menu hook worked!");
	ISBuildMenu.buildDoorMenuOld(subMenu,player);
	local sprite = ISBuildMenu.getWoodenDoorSprites(player);
	local doorsOption = subMenu:addOption("Latched Door", worldobjects, ISBuildMenu.onLatchedDoor, square, sprite, player);
	-- plank, nails, hinge, knob, wire, cSkill,options, player
--	local tooltip = ISBuildMenu.canBuild(5,8,2,1,0,3,doorsOption, player);
    --local tooltip = ISBuildMenu.canBuild(5,8,2,0,0,3,doorsOption, player);
	local tooltip = ISBuildMenu.canBuild(0,0,0,0,0,0,doorsOption, player);
--	tooltip:setName(getText("ContextMenu_Wooden_Door"));
	tooltip:setName("Latched Door");
	tooltip.description = "A latched door, has to be placed in a door frame " .. tooltip.description;
--	tooltip.description = getText("Tooltip_craft_woodenDoorDesc") .. tooltip.description;
	tooltip:setTexture(sprite.sprite);
end

-- TODO: it would be nice if I could take the items used from the recipe to add the doorknob hash?
ISBuildMenu.onLatchedDoor = function(worldobjects, square, sprite, player)
	-- sprite, northsprite, openSprite, openNorthSprite
	local door = ISLatchedDoor:new(sprite.sprite, sprite.northSprite, sprite.openSprite, sprite.openNorthSprite);
	door.modData["need:Base.Plank"] = "5";
	door.modData["need:Base.Nails"] = "8";
	door.modData["need:Base.Hinge"] = "2";
	--door.modData["need:Base.Doorknob"] = "1";
	door.player = player;
	getCell():setDrag(door, player);
end