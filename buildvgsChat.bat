@echo off
echo Building...
IF EXIST build (rm -R build)
IF EXIST vgsChat.zip (rm vgsChat.zip)
mkdir build
cp -R vgsChat build
cd build
zip -r vgsChat.zip vgsChat
cd ..
mv build\vgsChat.zip vgsChat.zip
grep -Eho "version\=([0-9\.]+)" vgsChat\mod.info | grep -Eho "[0-9\.]+" > build\t.txt
@rem sed -i 's/\./_/' build\t.txt
set /p TEMP_VERSION=<build\t.txt
mv vgsChat.zip vgsChat%TEMP_VERSION%.zip
rm -R build
set TEMP_VERSION=
echo Done.