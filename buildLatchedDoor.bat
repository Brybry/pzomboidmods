@echo off
echo Building...
IF EXIST build (rm -R build)
IF EXIST latchedDoor.zip (rm latchedDoor.zip)
mkdir build
cp -R latchedDoor build
rm -R build\latchedDoor\media\lua\shared
cd build
zip -r latchedDoor.zip latchedDoor
cd ..
mv build\latchedDoor.zip latchedDoor.zip
rm -R build
echo Done.