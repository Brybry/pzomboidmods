local customSlot = { "ClothingItem_Head", "ClothingItem_Face", "ClothingItem_Neck", "ClothingItem_LeftShoulder", 
    "ClothingItem_RightShoulder", "ClothingItem_Overcoat", "ClothingItem_Undershirt", "ClothingItem_Arms", 
    "ClothingItem_Gloves", "ClothingItem_Waist", "ClothingItem_Legs" };

-- TODO: for having multiple slots of the same type allow for tables in customSlot with an optional integer value

local function unequipFromCustomSlot(character, item)
    if not character or not item then return; end
    local iModData = item:getModData();
    local cModData = character:getModData();

    if not iModData["ClothingItem_Slot"] then
        return;
    end
    local slot = iModData["ClothingItem_Slot"];
    local slotType = customSlot[slot];
    iModData["Equipped"] = false;
    cModData[slotType] = false;
    item:setKeyId(-1);

    -- unset item equip effects
    item:setActualWeight(item:getWeight());
end

local function equipInCustomSlot(character, item)
    if not character or not item then return; end

    local iModData = item:getModData();
    local cModData = character:getModData();

    if not iModData["ClothingItem_Slot"] then
        return;
    end
    
    local slot = iModData["ClothingItem_Slot"];
    local slotType = customSlot[slot];

    -- find and unequip old waist item
    if cModData[slotType] then
        local oldItem = nil;
        local items = character:getInventory():getItems();
        for i=0, items:size()-1 do
            local o = items:get(i);
            if o:getKeyId() == cModData[slotType] and o:getModData()["ClothingItem_Slot"] then
                oldItem = o;
                break;
            end
        end
        if oldItem then
            unequipFromCustomSlot(character, oldItem);
        else
            print("beltMod Error: old equipped " .. slotType .. "item not found.");
        end
    end
    
    item:setKeyId(ZombRand(100000000)+1);
    iModData["Equipped"] = true;
    cModData[slotType] = item:getKeyId();

    -- set item equip effects
    item:setActualWeight(item:getWeight()*0.3);

    if instanceof(item, "InventoryContainer") then
        -- add 'backpack' button
        getPlayerData(character:getPlayerNum()).playerInventory:refreshBackpacks();
    end
end

-- TODO: Clean this up and make it more efficient
local ISInventoryPage_refreshBackpacks_old = ISInventoryPage.refreshBackpacks;
function ISInventoryPage:refreshBackpacks(...)
    local ret = ISInventoryPage_refreshBackpacks_old(self, ...);
    local oldNumBackpacks = #self.backpacks; -- unused?
    local c = #self.backpacks + 1;
    local foundIndex = -1;
    local found = false;
    local containerButton = nil;

    local playerObj = getSpecificPlayer(self.player); -- unused?


    if not self.onCharacter then return; end;
   
    local items = getSpecificPlayer(self.player):getInventory():getItems();
    for i = 0, items:size()-1 do
        local item = items:get(i);
        local iModData = item:getModData();
        if item:getCategory() == "Container" and iModData["Equipped"] and iModData["ClothingItem_Slot"] then
            -- hack to simulate weight reduction
            local weightReduction = item:getWeightReduction() / 100.0;
            item:setActualWeight(item:getWeight()*0.3 - item:getContentsWeight()*weightReduction);
            
            -- found a container, so create a button for it...
            local containerButton = ISButton:new(self.width-32, ((c-1)*32)+15, 32, 32, "", self, ISInventoryPage.selectContainer, ISInventoryPage.onBackpackMouseDown, true);
            containerButton:setImage(item:getTex());
            containerButton:forceImageSize(30, 30)
            containerButton.anchorBottom = false;
            containerButton:setOnMouseOverFunction(ISInventoryPage.onMouseOverButton);
            containerButton:setOnMouseOutFunction(ISInventoryPage.onMouseOutButton);
            containerButton.anchorRight = true;
            containerButton.anchorTop = false;
            containerButton.anchorLeft = false;
            containerButton:initialise();
            containerButton.borderColor.a = 0.0;
            containerButton.backgroundColor.a = 0.0;
            containerButton.backgroundColorMouseOver = {r=0.3, g=0.3, b=0.3, a=1.0};
            containerButton.inventory = item:getInventory();
            containerButton.capacity = item:getEffectiveCapacity(playerObj);
            containerButton.name = item:getName();
            if self.inventoryPane.inventory == containerButton.inventory then
                containerButton.backgroundColor = {r=0.7, g=0.7, b=0.7, a=1.0};
                --foundIndex = c;
                --found = true;
            end
            self:addChild(containerButton);
            self.backpacks[c] = containerButton;
            c = c + 1;
        end
    end

    
    -- Unsure how much of the below is needed
	self.inventoryPane.inventory = self.inventoryPane.lastinventory;
	self.inventory = self.inventoryPane.inventory;

    -- Search through backpacks for currently selected backpack
    for k = 1, c-1 do
--        print("Searching" .. k);
        if self.inventoryPane.inventory == self.backpacks[k].inventory then
            foundIndex = k;
            found = true;
            break;
        end
    end
    
    if self.backpackChoice ~= nil and getSpecificPlayer(self.player):getJoypadBind() ~= -1 then
        if self.backpackChoice >= c then
	        if #self.backpacks > 1 then
		        self.backpackChoice = 2;
	        else
		        self.backpackChoice = 1;
	        end

        end
        if self.backpacks[self.backpackChoice] ~= nil then
            self.inventoryPane.inventory = self.backpacks[self.backpackChoice].inventory;
        end
    else
        if not self.onCharacter and oldNumBackpacks == 1 and c > 1 then
            self.inventoryPane.inventory = self.backpacks[1].inventory;
            self.capacity = self.backpacks[1].capacity
	    elseif found then
            self.capacity = self.backpacks[foundIndex].capacity
	    elseif not found and c > 1 then
            if self.backpacks[1] and self.backpacks[1].inventory then
                self.inventoryPane.inventory = self.backpacks[1].inventory;
                self.capacity = self.backpacks[1].capacity
            end
        elseif self.inventoryPane.lastinventory ~= nil then
            self.inventoryPane.inventory = self.inventoryPane.lastinventory;
        end
    end

    if not found then
        self.toggleStove:setVisible(false);
    end
	self.inventoryPane:bringToTop();

    self.resizeWidget2:bringToTop();
    self.resizeWidget:bringToTop();

    self.inventory = self.inventoryPane.inventory;

    for k,containerButton in ipairs(self.backpacks) do
        if containerButton.inventory == self.inventory then
            containerButton.backgroundColor = {r=0.7, g=0.7, b=0.7, a=1.0}
        else
            containerButton.backgroundColor.a = 0
        end
    end

    if self.inventoryPane ~= nil then self.inventoryPane:refreshContainer(); end
	self:refreshWeight();

	self:syncToggleStove()

    return ret;
end

local ISWearClothing_perform_old = ISWearClothing.perform;
function ISWearClothing:perform(...)
    --print("Hook ISWearClothing_perform_old");
    if self.item:getModData()["ClothingItem_Slot"] then
        self.item:getContainer():setDrawDirty(true);
        self.item:setJobDelta(0.0);

        equipInCustomSlot(self.character, self.item);

        ISBaseTimedAction.perform(self);
        return;
    end
    
    -- this will error if not container and not clothing with body location and that's why the return above + duplicate code
    return ISWearClothing_perform_old(self, ...);
end

local ISUnequipAction_perform_old = ISUnequipAction.perform;
function ISUnequipAction:perform(...)
    --print("Hook ISUnequipAction_perform_old");
    if self.item:getModData()["ClothingItem_Slot"] then
        unequipFromCustomSlot(self.character, self.item);
    end
    return ISUnequipAction_perform_old(self, ...);
end

local ISInventoryTransferAction_removeItemOnCharacter_old = ISInventoryTransferAction.removeItemOnCharacter;
function ISInventoryTransferAction:removeItemOnCharacter(...)
    if self.item:getModData()["ClothingItem_Slot"] and self.item:getModData()["Equipped"] then
        unequipFromCustomSlot(self.character, self.item);
    end
    return ISInventoryTransferAction_removeItemOnCharacter_old(self, ...);
end

local ISInventoryPane_renderdetails_old = ISInventoryPane.renderdetails;
-- lua/client/ISUI/ISInventoryPane.lua:1467
-- not using var args because lazy
-- TODO: Clean this up and make it more efficient, duplicated a lot of unneeded code
function ISInventoryPane:renderdetails(doDragged)
    local ret = ISInventoryPane_renderdetails_old(self, doDragged);
    --print("Hook ISInventoryPane_renderdetails_old");
    local y = 0;
    -- for each item stack
    for k, v in ipairs(self.itemslist) do
        local count = 1;
        -- for each item in the stack
        for k2, v2 in ipairs(v.items) do
            local item = v2;
            local doIt = true;
            local xoff = 0;
            local yoff = 0;
            local tex = item:getTex();


            if self.dragging ~= nil and self.selected[y+1] ~= nil and self.dragStarted then
                xoff = self:getMouseX() - self.draggingX;
                yoff =  self:getMouseY() - self.draggingY;
                if not doDragged then
                    doIt = false;
                end
            else
                if doDragged then
                    doIt = false;
                end
            end

            if tex ~= nil and doIt then
                local texDY = 1
                local texWH = math.min(self.itemHgt-2,32)
                local auxDXY = math.ceil(20 * self.texScale)

                if count == 1 then
                    if item:getModData()["ClothingItem_Slot"] and item:getModData()["Equipped"] then
                        self:drawTexture(self.equippedItemIcon, (10+auxDXY+xoff), (y*self.itemHgt)+16+auxDXY+yoff, 1, 1, 1, 1);
                    end
                elseif v.count > 2 or (doDragged and count > 1 and self.selected[(y+1) - (count-1)] == nil) then
                    if item:getModData()["ClothingItem_Slot"] and item:getModData()["Equipped"] then
                        self:drawTexture(self.equippedItemIcon, (10+auxDXY+16+xoff), (y*self.itemHgt)+16+auxDXY+yoff, 1, 1, 1, 1);
                    end
                end
            end
            
            y = y + 1;

            if count == 1 and self.collapsed ~= nil and v.name ~= nil and self.collapsed[v.name] then
               break;
            end
            count = count + 1;
        end
    end
    return ret;
end

local ISInventoryPaneContextMenu_unequipItem_old = ISInventoryPaneContextMenu.unequipItem;
ISInventoryPaneContextMenu.unequipItem = function(item, player)
    --print("Hook ISInventoryPaneContextMenu_unequipItem_old");
    if item:getModData()["ClothingItem_Slot"] and item:getModData()["Equipped"] then
        ISTimedActionQueue.add(ISUnequipAction:new(getSpecificPlayer(player), item, 50));
        return;
    end
    return ISInventoryPaneContextMenu_unequipItem_old(item, player);
end


-- TODO: worried about potential bugs with duplicate wear/unequip buttons
function HandleCustomSlotContextMenu(player, context, items)
    local isAllCustomSlots = true;
    local unequip = false;

    for i,item in ipairs(items) do
        local testItem = item;
        if not instanceof(item, "InventoryItem") then
            testItem = item.items[1];
        end

        if not testItem:getModData()["ClothingItem_Slot"] then
            isAllCustomSlots = false;            
        else
            --print("ClothingItem_Waist: "..tostring(testItem:getModData()["ClothingItem_Waist"]));
            --print("Equipped: "..tostring(testItem:getModData()["Equipped"]));
            if testItem:getModData()["Equipped"] then
                unequip = true;
            end
        end
    end

    if isAllCustomSlots and not unequip then
        context:addOption(getText("ContextMenu_Wear"), items, ISInventoryPaneContextMenu.onWearItems, player);
    elseif unequip then
        context:addOption(getText("ContextMenu_Unequip"), items, ISInventoryPaneContextMenu.onUnEquip, player);  
    end

end
Events.OnFillInventoryObjectContextMenu.Add(HandleCustomSlotContextMenu);
